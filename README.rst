Glider Tools
============

.. image:: https://travis-ci.com/GliderToolsCommunity/GliderTools.svg?branch=master
        :target: https://travis-ci.com/GliderToolsCommunity/GliderTools
.. image:: https://badgen.net/pypi/v/glidertools
        :target: https://pypi.org/project/glidertools
.. image:: https://readthedocs.org/projects/glidertools/badge/?version=latest
        :target: https://glidertools.readthedocs.io
.. image:: https://img.shields.io/badge/License-GPLv3-blue.svg
        :target: https://www.gnu.org/licenses/gpl-3.0
.. image:: https://coveralls.io/repos/github/GliderToolsCommunity/GliderTools/badge.svg
        :target: https://coveralls.io/github/GliderToolsCommunity/GliderTools

Glider tools is a Python 3.6+ package designed to process data from the first level of processing
to a science ready dataset. The package is designed to easily import data to a standard column
format (numpy.ndarray or pandas.DataFrame). Cleaning and smoothing functions are flexible and can
be applied as required by the user. We provide examples and demonstrate best practices as
developed by the http://socco.org.za/.

GliderTools has moved to https://github.com/GliderToolsCommunity/GliderTools
----------------------------------------------------------------------------